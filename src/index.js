const cookieJs = require("cookie")
const { createProxyMiddleware } = require("http-proxy-middleware")

// Simple volatile storage
const sessions = {}

// Not for production
const makeRandomString = () => Math.random().toString(36).substring(7).padEnd(6, 0)

function addSession(user)
{
	const sessionId = makeRandomString()
	sessions[sessionId] = user
	return sessionId
}

function mergeDefaultConfig(config)
{
	const defaultConfig =
	{
		auth:
		{
			presetUser: { name: "test" },
			autoLogin: false,
			encode: JSON.stringify,
			headerName: "authorization",
			serverPath: "/local-server",
			loginPath: "/.auth/login",
			successPath: "/.auth/success",
			logoutPath: "/.auth/logout",
			redirectParamName: "redirect",
		},
		proxy:
		{
			target: "http://localhost:8080",
		},
		sessionCookie:
		{
			name: "session",
			maxAge: 1000 * 60 * 60 * 24 * 7,
		},
		// Todo: support a fully custom registerer called after all
	}

	for(const [k, v] of Object.entries(defaultConfig))
	{
		config[k] = {...v, ...config?.[k]}
	}

	if(!Array.isArray(config.auth.presetUser))
	{
		config.auth.presetUser = [config.auth.presetUser] // Put in an array for consistency
	}

	if(!Array.isArray(config.auth.serverPath))
	{
		config.auth.serverPath = [config.auth.serverPath] // Put in an array for consistency
	}
}



module.exports = iConfig =>
{
	const config = iConfig || {}
	mergeDefaultConfig(config)

	// From a matching sessionId in the cookie or undefined
	function getUser(cookie)
	{
		const cookies = cookieJs.parse(cookie || "")
		const sessionId = cookies[config.sessionCookie.name]
		return [sessions[sessionId], sessionId]
	}

	function tradeSessionForJwt(proxyReq, req, res)
	{
		const [user, sessionId] = getUser(proxyReq.getHeaders().cookie)
		if(user)
		{
			console.log("User found for sessionId", sessionId)
			proxyReq.setHeader(config.auth.headerName, config.auth.encode(user))
		}
		else
		{
			// Redirect to the login page
			res.statusCode = 302

			// For raw API calls, encode the redirect url here
			// It can be overriden by the 'referer' web page after.
			const loginParams = new URLSearchParams()
			loginParams.set(config.auth.redirectParamName, req.url)
			let nextLocation = config.auth.loginPath

			// Simply bypass the login page
			if(config.auth.autoLogin)
			{
				loginParams.set("user", JSON.stringify(config.auth.presetUser[0]))
				nextLocation = config.auth.successPath
			}

			res.setHeader("location", nextLocation + "?" + loginParams)
			res.end()
		}
	}

	const registerer = app =>
	{
		for(const srvPath of config.auth.serverPath)
		{
			app.use(
				srvPath,
				createProxyMiddleware({
					...config.proxy,
					onProxyReq: tradeSessionForJwt,
				})
			)
		}
		// Make an HTML + JS page in JS: JS-ception
		app.use(
			config.auth.loginPath,
			(req, res) =>
			{
				console.log("New login")
				res.setHeader("content-type", "text/html")
				res.send(`
					<html>
						<head>
							<title>React auth proxy</title>
							<link rel="icon" href="data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'><text y='0.9em' font-size='90'>🛡️</text></svg>">
							<style>
							html, body { margin: 0px; }
							h1 {
								text-align: center;
								padding: 18px;
								background: linear-gradient(to right, #dff, #5ae);
							}
							button {
								margin: 10px;
							}
							</style>
						</head>
						<body>
							<h1>👥 Login page</h1>
							${
	config.auth.presetUser.map(u => "<button onclick='login(" + JSON.stringify(u) + ")'>Login as " + Object.values(u)[0] + "</button>\n").join("<br/>\n")
}

							<script>
								function login(user)
								{
									console.log("Login action")
									const successParams = new URLSearchParams(location.search)

									successParams.set("user", JSON.stringify(user))
									location.replace("${config.auth.successPath}?" + successParams)
								}
							</script>
						</body>
					</html>
				`)
			}
		)

		app.get(
			config.auth.successPath,
			(req, res) =>
			{
				console.log("Success, enroll the user then go back", req.query)
				const newSessionId = addSession(JSON.parse(req.query.user))
				console.log("Create a user for sessionId", newSessionId, config.sessionCookie)

				res.statusCode = 302
				res.cookie(config.sessionCookie.name, newSessionId, {maxAge: config.sessionCookie.maxAge})
				res.setHeader("location", req.query[config.auth.redirectParamName])

				res.end()
			}
		)

		app.use(
			config.auth.logoutPath,
			(req, res) =>
			{
				console.log("Logout")
				res.clearCookie(config.sessionCookie.name)

				const redirectUrl = req.query.post_logout_redirect_uri
				if(redirectUrl)
				{
					res.statusCode = 302
					res.setHeader("location", redirectUrl)
					res.end()
				}
				else
				{
					res.send("done")
				}
			}
		)
		config.proxy.callback && config.proxy.callback(app)
	}

	// To access the resolved config
	registerer.config = config
	registerer.getUser = getUser
	registerer.tradeSessionForJwt = tradeSessionForJwt

	return registerer
}

//Additional named exports
module.exports.sessions = sessions
