const express = require("express")
const proxy = require("../src/index.js")

function main()
{
	const apiServerPort = 8080
	const apiServer = express()
	apiServer.use("/local-server", (req, res) =>
	{
		console.log("In the server")
		const user = JSON.parse(req.headers[registerer.config.auth.headerName] || "null")
		console.log("the user:", user)
		res.json(user)
	})

	const proxyServerPort = 3000
	const proxyServer = express()

	const registerer = proxy({
		auth: {
			presetUser: [
				{ name: "Carlo", role: "account-manager" },
				{ name: "Nobody", role: "user-manager" },
			]
		}
	})
	registerer(proxyServer)

	apiServer.listen(apiServerPort, () =>
	{
		console.log("api-server listening on http://localhost:" + apiServerPort)
		proxyServer.listen(proxyServerPort, () =>
		{
			console.log("proxy-server listening on http://localhost:" + proxyServerPort)
		})
	})
}

main()
